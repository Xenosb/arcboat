package com.fer.arcboat;


import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Set;
import java.util.UUID;
import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.app.Dialog;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.DialogInterface;
import android.content.Context;

/**
 * This is the main activity of the ArcBoat mobile application.
 * 
 * Application allows users to control a model of remotely controlled
 * boat. Control is acquired through orientation or gyro sensor and 
 * the communication through standard Bluetooth protocol. 
 * 
 * Users can control the model by rotating the mobile device around
 * pitch and roll axes. User interface displays orientation values
 * alongside with the state of the communication protocol, model boat
 * state and readings form distance sensor located on the model. Users 
 * can also establish the connection from inside of the application 
 * as well as change slowing and stopping distances.
 * 
 * @author Marko Kostervajn, Dario Mili�i�, Bruno Vunderl (leader)
 * 
 *
 * ArcBoat is main class of the application.
 * Implements all of the application features.
 */
public class ArcBoat extends Activity implements SensorEventListener {
	
	private SensorManager sensorManager;
	public BluetoothAdapter mBluetoothAdapter = null;
	public BluetoothDevice arduinoDevice;
	public BluetoothSocket arduinoSocket;
	public OutputStream arduinoOutputStream;
	public InputStream arduinoInputStream;
	public Boolean connectedArduino=false, stopWorker=false, streamsReady=false;
	public Thread workerThread;
	final Context context = this;
	
	
	// Declaration of global objects and variables
	TextView distanceLabel, statusLabel, directionLabel, speedLabel, connectionStatus;
	ProgressBar pitchBar, rollBar;
	float xc=0, yc=0, zc=0, xd=0, yd=0, zd=0;
	int xp=0, yp=0, zp=0, slowdownValue=25, stoppingValue=10;
	byte[] readBuffer;
	int readBufferPosition;
	String speedString="", directionString="", slowdownString="", stoppingString="";
	
	
	
	/** 
	 * Called when application is created
	 */
    @SuppressWarnings("deprecation")
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_arc_boat);

        /** 
         * Prevents screen form going to sleep
         */
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        
        /** 
         * Gets Bluetooth handle if device supports Bluetooth
         */
		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            Toast.makeText(this, "Bluetooth is not available", Toast.LENGTH_LONG).show();
            finish();
            return;
        }
        
        
		/** 
		 * Creates text views
		 */
		distanceLabel = (TextView)findViewById(R.id.distanceLabel);
		statusLabel = (TextView)findViewById(R.id.statusLabel);
		directionLabel = (TextView)findViewById(R.id.directionLabel);
		speedLabel = (TextView)findViewById(R.id.speedLabel);
		connectionStatus = (TextView)findViewById(R.id.connectionStatus);
				
		
		/**
		 *  Creates progress bars
		 */
		pitchBar=(ProgressBar) findViewById(R.id.progressBar1);
		rollBar=(ProgressBar) findViewById(R.id.progressBar2);
		
		/**
		 *  Adds sensor listener
		 */
		sensorManager=(SensorManager)getSystemService(SENSOR_SERVICE);
		sensorManager.registerListener(this,
				sensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION),
				SensorManager.SENSOR_DELAY_NORMAL);
		
		
		/**
		 *  Calibrate button functions
		 */
        Button button = (Button) findViewById(R.id.reset);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                xd=xc;
                yd=yc;
                zd=zc;
            }   
        });
        
    }
    
	/**
	 *  Called when application goes to background
	 */
	public void onPause() {
		disconnectArduino();
		super.onPause();
	}
	
	/**
	 *  Called when application is pulled to foreground
	 * @param savedInstanceState
	 */
	public void onResume(Bundle savedInstanceState) {
		super.onResume();
		try {
			connectArduino();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
    
        
    /**
     *  Inflates menu form /menu xml files
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_arc_boat, menu);
        return true;
    }
 
    
    
    /**
     *  Menu options
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	
    	switch (item.getItemId()) {
    		/*
    	    // Bluetooth enable/disable option
        	case R.id.btMenu:
                if (!mBluetoothAdapter.isEnabled()) {
                	Toast.makeText(this, "Turning bluetooth ON", Toast.LENGTH_SHORT).show();          
                	mBluetoothAdapter.enable();                  
                }
                else
                	Toast.makeText(this, "Turning bluetooth OFF", Toast.LENGTH_SHORT).show();
                	if (connectedArduino==true) disconnectArduino();
                	mBluetoothAdapter.disable();
                	connectedArduino = false;
        		return true;
        	*/
    	
        	/**
        	 *  Connect/Disconnect option	
        	 */
        	case R.id.connectMenu:
        		if (!mBluetoothAdapter.isEnabled()) Toast.makeText(this, "Bluetooth is disabled", Toast.LENGTH_SHORT).show();
        		else {
        			if (connectedArduino==true) disconnectArduino();
        			else if (findDevice()) {
	        			Toast.makeText(this, "ArCboat found", Toast.LENGTH_SHORT).show();
	        			try {
							connectArduino(); } 
	        			catch (IOException e) {
	        				Toast.makeText(this, "Couldn't establish a connection", Toast.LENGTH_SHORT).show();
							e.printStackTrace(); }
	        		}
	        		else Toast.makeText(this, "Requested device is not avaiable", Toast.LENGTH_SHORT).show();
        		}
        		return true;
        	
        	/**
        	 *  Dialog with limits
        	 */
        	case R.id.limitMenu:
        		final Dialog dialog = new Dialog(context);
        		dialog.setContentView(R.layout.limit_dialog);
        		dialog.setTitle("Limit dialog");
        		
        		final NumberPicker slowdownPicker = (NumberPicker) dialog.findViewById(R.id.slowdownPicker);
        		final NumberPicker stoppingPicker = (NumberPicker) dialog.findViewById(R.id.stopPicker);        		
        		Button dialogButton = (Button) dialog.findViewById(R.id.limitButton);
        		
        		String[] nums = new String[60];
        	    for(int i=0; i<nums.length; i++)
        	           nums[i] = Integer.toString(i);

        	    slowdownPicker.setMinValue(1);
        	    slowdownPicker.setMaxValue(60);
        	    slowdownPicker.setWrapSelectorWheel(true);
        	    slowdownPicker.setDisplayedValues(nums);
        	    slowdownPicker.setValue(slowdownValue+1);
        	    
        	    stoppingPicker.setMinValue(1);
        	    stoppingPicker.setMaxValue(60);
        	    stoppingPicker.setWrapSelectorWheel(false);
        	    stoppingPicker.setDisplayedValues(nums);
        	    stoppingPicker.setValue(stoppingValue+1);
        		
        		dialogButton.setOnClickListener(new OnClickListener() {
    				public void onClick(View v) {
    					slowdownValue = slowdownPicker.getValue() - 1;
    					stoppingValue = stoppingPicker.getValue() - 1;
    					dialog.dismiss();
    				}
    			});
        		
        		dialog.show();
        		return true;
        		
        	// Default choice
        	default:
        		return super.onOptionsItemSelected(item);
    	}
    }
    
    
    
    /**
     *  Method for disconnecting Arduino
     */
    private void disconnectArduino() {
    	try {
    		connectionStatus.setText("DISCONNECTED");
    		connectedArduino=false;
			arduinoOutputStream.close();						
			arduinoInputStream.close();
			arduinoSocket.close();
			distanceLabel.setText("");
			speedLabel.setText("");
			statusLabel.setText("");
			directionLabel.setText("");
			speedString="";
			directionString="";
			Toast.makeText(this, "Arduino disconnected", Toast.LENGTH_SHORT).show();
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
    
    /**
     *  Connects and creates stream to Arduino
     * @throws IOException
     */
    private void connectArduino() throws IOException {    	
    	if (!mBluetoothAdapter.isEnabled()) Toast.makeText(this, "Bluetooth is disabled", Toast.LENGTH_SHORT).show();
    	else if (connectedArduino==true) Toast.makeText(this, "Device already connected", Toast.LENGTH_SHORT).show();
    	else {
	    	UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb"); // Typical SerialPortService ID
	        arduinoSocket = arduinoDevice.createRfcommSocketToServiceRecord(uuid);
	        arduinoSocket.connect();
	        arduinoOutputStream = arduinoSocket.getOutputStream();
	        arduinoInputStream = arduinoSocket.getInputStream();	
	        
	        connectedArduino = true;        
	    	connectionStatus.setText("CONNECTED");
	        
	        beginListenForData();
	        streamsReady=true;
		}
    }


	/**
	 *  Looks if Arduino is paired
	 * @return Boolean - Arduino paired or not
	 */
	private boolean findDevice() {
		Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
        if (pairedDevices.size() > 0) {
            for (BluetoothDevice device : pairedDevices) {
                if (device.getName().equals("ArduiN")) {
                    arduinoDevice = device;
                    return true; }
            }
        }
        else Toast.makeText(this, "No paired devices found", Toast.LENGTH_SHORT).show();
        return false;
	}

	
	/** 
	 * Listens incoming serial data
	 */
	void beginListenForData()
    {
        final Handler handler = new Handler(); 
        final byte delimiter = 10; // ASCII code for a newline character
        
        stopWorker = false;
        readBufferPosition = 0;
        readBuffer = new byte[2048];
        
        /**
         * Bluetooth data collection thread
         */
        workerThread = new Thread(new Runnable() {
            public void run() {       
            	
               while(!Thread.currentThread().isInterrupted() && !stopWorker) {
            	   
                    try {
                        int bytesAvailable = arduinoInputStream.available(); 
                        
                        if(bytesAvailable > 0) {
                        	
                            byte[] packetBytes = new byte[bytesAvailable];
                            arduinoInputStream.read(packetBytes);
                            
                            for(int i=0; i<bytesAvailable; i++) {
                                byte b = packetBytes[i];
                                
                                if(b == delimiter) {
                                	
                                    byte[] encodedBytes = new byte[readBufferPosition];
                                    System.arraycopy(readBuffer, 0, encodedBytes, 0, encodedBytes.length);
                                    final String inData = new String(encodedBytes, "US-ASCII");
                                    readBufferPosition = 0;
                                    
                                    
                                    /**
                                     * Parses the input stream and fills the appropriate labels
                                     */
                                    handler.post(new Runnable() {
                                        public void run() {
                                        	String data = inData.substring(0, inData.length()-1);
                                        	if (data.length()>0) {
	                                        	if (data.toCharArray()[0]=='h') 
	                                        		distanceLabel.setText(data.substring(1) +" cm");
	                                        	else if (data.equalsIgnoreCase("statSlow"))
	                                        		statusLabel.setText("Slowing");
	                                        	else if (data.equalsIgnoreCase("statStop"))
	                                        		statusLabel.setText("Stopped");
	                                        	else if (data.equalsIgnoreCase("statRun"))
	                                        		statusLabel.setText("Running");
	                                        	else if (data.length()>=7 && data.substring(0,6).equalsIgnoreCase("dirSet"))
	                                        		directionLabel.setText(data.substring(6));
	                                        	else if (data.length()>=7 && data.substring(0,6).equalsIgnoreCase("spdSet"))
	                                        		speedLabel.setText(data.substring(6));
                                        	}
                                        }
                                    });
                                    
                                }
                                
                                else {
                                    readBuffer[readBufferPosition++] = b;
                                }
                            }
                        }
                    }
                    
                    catch (IOException ex) {
                        stopWorker = true;
                    }
               }
            }
        });

        workerThread.start();
    }
	
	
	/**
	 *  Method for output stream to Arduino
	 *  @throws IOException
	 */
	public void sendData() throws IOException {
		arduinoOutputStream.flush();
		arduinoOutputStream.write(speedString.getBytes());
		arduinoOutputStream.write(directionString.getBytes());
		arduinoOutputStream.write(slowdownString.getBytes());
		arduinoOutputStream.write(stoppingString.getBytes());		
	}
	
	
	/**
	 *  Method that reads data from orientation sensor.
	 *  Fills orientation bars and output variables.
	 */
	@SuppressWarnings("deprecation")
	public void onSensorChanged(SensorEvent event){
		
		/**
		 *  Checks sensor type
		 */
		if(event.sensor.getType()==Sensor.TYPE_ORIENTATION){
			/**
			 *  Sets sensor values
			 */
			xc=event.values[0];
			yc=event.values[1];
			zc=event.values[2];
			
			/**
			 *  Calculates differences
			 */
			xp=(int) (xc-xd);
			yp=(int) (yc-yd);
			zp=(int) (zc-zd);
			 
			/**
			 *  Fills progress bars
			 */
			pitchBar.setProgress(zp+90);
			rollBar.setProgress(yp+90);
			
			/**
			 *  Generatse direction and speed strings
			 */
			speedString = "s" + Integer.toString(Math.abs(zp)*3) + "\r";
			directionString = "d" + Integer.toString(yp) + "\r";
			slowdownString = "x" + Integer.toString(slowdownValue) + "\r";
			stoppingString = "y" + Integer.toString(stoppingValue) + "\r";
			
			if (streamsReady) {
				try {
					sendData();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
		}
		
		else {
			/**
			 *  Shows alert dialog and closes app if there is no gyro
			 */
			AlertDialog alertDialog = new AlertDialog.Builder(this).create();
			alertDialog.setTitle("Gyroscope alert");
			alertDialog.setMessage("Gyroscope sensor function not detected");
			alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
			   public void onClick(final DialogInterface dialog, final int which) {
				   dialog.dismiss();
			      finish();
			   }
			});
			alertDialog.setIcon(R.drawable.ic_launcher);
			alertDialog.show();
		}
	}

	
	/**
	 *  Has to be declared to ensure work of Orientation sensor
	 */
	public void onAccuracyChanged(Sensor arg0, int arg1) {	
	}
	
}
