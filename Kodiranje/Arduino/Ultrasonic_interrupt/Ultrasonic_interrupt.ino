/** Ultrasonic interrupt
 * ---------------------
 *
 * Očitava vrijednost sa HC-SR04 senzora 
 * te podatke šalje na serijsku vezu i motor
 *
 * @author: Bruno Vunderl
 */


// External libraries
#include <NewPing.h>


// Global definitions
#define greeting "Yaaawn! Good morning. Let's try how this program works"
#define TIPpin 5
#define TriggerPin 3
#define EchoPin 4
#define PredefinedDistance 20 // The distance boat starts slowing down


// Global variables
static int MotorValue = 0, UsDistance = 0, UsDistanceOld = 0;
unsigned int pingSpeed = 250; // Frequeny of HS sensor refresh time
unsigned long pingTimer; 


// Linked components
NewPing ultrasonic(TriggerPin, EchoPin); 


// Function called to slow down or stop the boat
void slowDown() {
  if (UsDistanceOld > PredefinedDistance) Serial.println("Slowing down");
  if (UsDistance <= 5) {
    if (MotorValue!=0) Serial.println("Stopped");
    MotorValue = 0;
  }
  else MotorValue = map(UsDistance, 5, PredefinedDistance, 0, 255);
}


// Interrupt function 
void TimerInt() {
  Serial.print("HC-SR04: ");
  UsDistanceOld = UsDistance;
  UsDistance = ultrasonic.ping_result / US_ROUNDTRIP_CM;
  Serial.print(UsDistance);
  Serial.println("cm");
  if (UsDistance < PredefinedDistance) slowDown(); 
  else {
    MotorValue=255;
    if (UsDistanceOld < PredefinedDistance) Serial.println("Stopped slowing down");
  }
}


// System setup
void setup() {
  pinMode(TIPpin, OUTPUT);
  Serial.begin(115200);
  Serial.println(greeting);
  pingTimer = millis();
}


// Work loop
void loop() {
  if (millis() >= pingTimer) {
    pingTimer += pingSpeed;      
    ultrasonic.ping_timer(echoCheck);
  }
  analogWrite(TIPpin, MotorValue);
}


// Timer2 interrupt function
void echoCheck() { 
  if (ultrasonic.check_timer()) TimerInt();
}
