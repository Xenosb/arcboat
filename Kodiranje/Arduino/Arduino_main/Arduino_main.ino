/** Arduino main
 * -------------
 *
 *
 * Main Arduino program
 *
 *
 * Receives Bluetooth data and 
 * controls direction and speed 
 * of the boat.
 *
 * Sensor readings are returned 
 * to controlling unit.
 *
 * @author: Bruno Vunderl
 *
 *
 * -------------
 */



/*
==================
External libraries
==================
*/
#include <NewPing.h>
#include <Servo.h>




/*
==================
Global definitions
==================
*/
#define TIPpin 6
#define TriggerPin 2
#define EchoPin 3
#define LedPin 13
#define ServoPin 9

#define MinimalPower 30  // Minimal power value on the motor
#define PingSpeed 80 // Frequeny of HS sensor refresh time 




/*
================
Global variables
================
*/
char character;
static int inNum;
static int motorValue = 0, motorValueSet = 0;
static int UsDistance = 0, UsDistanceOld = 0;
static int directionValue = 0;
static int activityCheck = 0;
String inputString = "";
boolean stringComplete = false, connectedAndroid = false;
unsigned long pingTimer; 
int PredefinedDistance = 25; // The distance boat starts slowing down
int StoppingDistance = 10; // The distance boat stops the motor




/*
=================
Linked components
=================
*/
NewPing ultrasonic(TriggerPin, EchoPin);
Servo servoOne;



/*
====================================
Simple blink procedure for debugging
====================================
*/
void blinkLed(int n) {
    for (int i=0; i<n; i++) {
        digitalWrite(LedPin, HIGH);
        delay(100);
        digitalWrite(LedPin, LOW);
        delay(100);
    }
}




/*
==================
Interrupt function
==================
*/
void TimerInt() {
    activityCheck++;
    
    if (activityCheck < 20) {
        if (!servoOne.attached()) servoOne.attach(ServoPin);
        UsDistanceOld = UsDistance;
        UsDistance = ultrasonic.ping_result / US_ROUNDTRIP_CM;
        Serial.println("h" + String(UsDistance));
        
        
        if (UsDistance < StoppingDistance && directionValue<160 && directionValue>80) {
            motorValue=0; 
            Serial.println("statStop");
        }
        
        else if (UsDistance < PredefinedDistance) {
            motorValue = map(UsDistance, StoppingDistance, PredefinedDistance, min(MinimalPower, motorValueSet), motorValueSet); 
            Serial.println("statSlow");
        }
        
        else {
            if (motorValueSet < MinimalPower) 
                motorValue=0; 
            else 
                motorValue = motorValueSet;       
            Serial.println("statRun");
        }
    }
  
    else {
        motorValue=0;
        servoOne.detach();
    }
  
    analogWrite(TIPpin, motorValue);
    servoOne.write(directionValue);
}




/*
============
System setup
============
*/
void setup() {
    pinMode(LedPin, OUTPUT);  
    pinMode(TIPpin, OUTPUT);
    inputString.reserve(200);
    pingTimer = millis();
    servoOne.attach(ServoPin);
    Serial.begin(38400);
}




/*
=================
Main program loop 
=================
*/
void loop() {
  
    /*
    -----------------------------------------------
    If sentence is complete, execute string parsing
    -----------------------------------------------
    */
    if (stringComplete) {
        
        // Sets slow down distance
        if (inputString[0] == 'x') {
            String inputValue="";
         
            for (int i=1; i<=inputString.length(); i++)
                inputValue+=inputString[i];
                              
            PredefinedDistance = inputValue.toInt();
            //Serial.println("spdSet" + String(PredefinedDistance));           
        }
        
        // Sets stopping distance
        else if (inputString[0] == 'y') {
            String inputValue="";
         
            for (int i=1; i<=inputString.length(); i++)
                inputValue+=inputString[i];
                              
            StoppingDistance = inputValue.toInt();
            //Serial.println("dirSet" + String(StoppingDistance));            
        }
      
        // Speed value is read from the serial bus
        else if (inputString[0] == 's') {      
            String inputValue="";
          
            for (int i=1; i<=inputString.length(); i++)
                inputValue+=inputString[i];
          
            int serialSpeedValue = inputValue.toInt();
      
            if (serialSpeedValue > 255) 
                motorValueSet = 255;
            else 
                motorValueSet = serialSpeedValue;
            Serial.println("spdSet" + String(serialSpeedValue));
        }
    
        // Direction value is read from the serial bus
        else if (inputString[0] == 'd') {
            String inputValue="";
        
            for (int i=1; i<=inputString.length(); i++)
                inputValue+=inputString[i];
        
            int serialDirectionValue = inputValue.toInt();
            directionValue = map(serialDirectionValue, -90, 90, 60, 195);
            Serial.println("dirSet" + String(directionValue));
        }
        
            
        else {
        //Serial.println("errRead:\"" + inputString + "\"");  
        }
       
    // Resets activity check
    activityCheck=0;
    
    // Preparing for new input
    stringComplete = false;
    inputString = "";     
  }


  /*
  -------------------
  Echo response timer
  -------------------
  */
  if (millis() >= pingTimer && connectedAndroid == true) {
    pingTimer += PingSpeed;      
    ultrasonic.ping_timer(echoCheck);
  }
}




/*
===========================================
Software interrupt when Serial is available
===========================================
*/
void serialEvent() {
    char inChar;
    connectedAndroid = true;
  
    while (Serial.available()) {
        inChar = (char)Serial.read();
        if (inChar=='\r') {
            inputString.toLowerCase();
            stringComplete = true;
        }
        else {
          inputString += inChar;
        }
   }
}




/*
=========================
Timer2 interrupt function
=========================
*/
void echoCheck() { 
  if (ultrasonic.check_timer()) TimerInt();
}
