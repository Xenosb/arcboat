char character;
int inNum;
int ledPin = 13;
String inputString = "";
boolean stringComplete = false;


// simple blink procedure meant for debugging
void blinkLed(int n) {
  for (int i=0; i<n; i++) {
    digitalWrite(ledPin, HIGH);
    delay(100);
    digitalWrite(ledPin, LOW);
    delay(100);
  }
}


// sequence called on initialization
void setup()
{
  pinMode(ledPin, OUTPUT);  
  Serial.begin(38400);
  inputString.reserve(200);
}


// main program loop
void loop() {
  
  // if sentence is complete executes program
  if (stringComplete) {
    if (inputString == "l0") {
      Serial.println("LED OFF");
      digitalWrite(ledPin, LOW); }
    else if (inputString == "l1") {
      digitalWrite(ledPin, HIGH);
      Serial.println("LED ON"); }
    else if (inputString[0] == 's') {
      int value = (int) inputString[1] - 48;
      blinkLed(value);
      Serial.println("BLINKED");
    }
    else {
      Serial.println("\"Input string: " + inputString + "\"");  
    }
    
    // preparing for new input
    stringComplete = false;
    inputString = "";     
  }
}


// called as a software interrupt when Serial is available
void serialEvent() {
  char inChar;
  
  while (Serial.available()) {
    inChar = (char)Serial.read();
    if (inChar=='\r') {
      inputString.toLowerCase();
      stringComplete = true;
    }
    else {
      inputString += inChar;
    }
  }
}
